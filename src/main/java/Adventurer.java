public class Adventurer {
    private String name;
    private Integer x;
    private Integer y;
    private Orientation orientation;
    private String course;
    private Integer nbTreasure;

    public Adventurer(String name, Integer x, Integer y, Orientation orientation, String course) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.course = course;
        this.nbTreasure = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Orientation getOrientation() {
        return this.orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Integer getNbTreasure() {
        return nbTreasure;
    }

    public void setNbTreasure(Integer nbTreasure) {
        this.nbTreasure = nbTreasure;
    }

    public Boolean getTreasure(Integer treasure) {
        this.nbTreasure = nbTreasure + treasure;
        return true;
    }

    public void turn(Turn turn) {
        switch (turn) {
            case D:
                this.orientation = this.orientation.getNext();
                break;
            case G:
                this.orientation = this.orientation.getPrevious();
                break;
            default:
                System.out.println("error");
                break;
        }
    }

    public void move() {
        switch (orientation) {
            case E:
                this.x = x + 1;
                break;
            case S:
                this.y = y + 1;
                break;
            case O:
                if (x > 0)
                    x = x - 1;
                break;
            case N:
                if (y > 0)
                    this.y = y - 1;
                break;
            default:
                System.err.println("Orientation is not E, S, O, or N");
                break;
        }
    }

    @Override
    public String toString() {
        return "Adventurer{" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", orientation=" + orientation +
                ", course='" + course + '\'' +
                ", nbTreasure=" + nbTreasure +
                '}';
    }
}
