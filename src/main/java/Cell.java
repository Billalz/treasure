public class Cell {
    private Integer nbTreasure;
    private Boolean isMountain;
    private Boolean isBusy;

    public Cell() {
        nbTreasure = 0;
        isMountain = false;
        isBusy = false;
    }

    public Integer getNbTreasure() {
        return nbTreasure;
    }

    public void setNbTreasure(Integer nbTreasure) {
        this.nbTreasure = nbTreasure;
    }

    public Integer giveTreasure() {
        if (nbTreasure > 0) {
            nbTreasure = nbTreasure - 1;
            return 1;
        }
        return 0;
    }

    public Boolean getMountain() {
        return isMountain;
    }

    public void setMountain(Boolean mountain) {
        isMountain = mountain;
    }

    public Boolean getBusy() {
        return this.isBusy;
    }

    public void setBusy(Boolean busy) {
        this.isBusy = busy;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "nbTreasure=" + nbTreasure +
                ", isMountain=" + isMountain +
                ", isBusy=" + isBusy +
                '}';
    }
}
