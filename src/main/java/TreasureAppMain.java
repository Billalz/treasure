import java.util.List;

public class TreasureAppMain {

    public static void main(String[] args) {
        TreasureGame treasureGame = new TreasureGame();
        System.out.println("Open file input...");
        List<String> list = Utils.readFile("input");
        treasureGame.parse(list);
        System.exit(treasureGame.run());
    }
}
