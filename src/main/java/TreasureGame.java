import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class TreasureGame {
    private List<Adventurer> adventurers;
    private List<Cell> cells;
    private int dimX;
    private int dimY;
    private int nbTours;

    TreasureGame() {
        this.adventurers = new ArrayList<Adventurer>();
        this.cells = new ArrayList<Cell>();
        this.dimX = 0;
        this.dimY = 0;
        this.nbTours = 0;
    }

    public List<Adventurer> getAdventurers() {
        return adventurers;
    }

    public void setAdventurers(List<Adventurer> adventurers) {
        this.adventurers = adventurers;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public int getDimX() {
        return dimX;
    }

    public void setDimX(int dimX) {
        this.dimX = dimX;
    }

    public int getDimY() {
        return dimY;
    }

    public void setDimY(int dimY) {
        this.dimY = dimY;
    }

    public int getNbTours() {
        return nbTours;
    }

    public void setNbTours(int nbTours) {
        this.nbTours = nbTours;
    }

    void parse(List<String> lines) {
        System.out.println("Parsing file input...");
        for (String line : lines) {
            if (Pattern.compile("^C - [0-9]{1,4} - [0-9]{1,4}\\s*").matcher(line).matches()) {
                System.out.println(line);
                String[] array = line.split(" - ");
                this.dimX = Integer.parseInt(array[1]);
                this.dimY = Integer.parseInt(array[2]);
                int i = dimX * dimY;
                while (i > 0) {
                    cells.add(new Cell());
                    i--;
                }
            } else if (Pattern.compile("^M - [0-9]{1,4} - [0-9]{1,4}\\s*").matcher(line).matches()) {
                String[] array = line.split(" - ");
                try {
                    this.getCell(Integer.parseInt(array[1]), Integer.parseInt(array[2])).setMountain(true);
                } catch (NullPointerException e) {
                    System.err.println("Commands lines are not correct, M is outside map or there is no map command \"C - {number} - {number}\"");
                }
            } else if (Pattern.compile("^T - [0-9]{1,4} - [0-9]{1,4} - [0-9]{1,4}\\s*").matcher(line).matches()) {
                String[] array = line.split(" - ");
                Integer x = Integer.parseInt(array[1]);
                Integer y = Integer.parseInt(array[2]);
                Integer nbTreasure = Integer.parseInt(array[3]);
                try {
                    this.getCell(x, y).setNbTreasure(nbTreasure);
                } catch (NullPointerException e) {
                    System.err.println("Commands lines are not correct, T is outside map or there is no map command \"C - {number} - {number}\"");
                }
            } else if (Pattern.compile("^A - [A-Za-z]{1,50} - [0-9]{1,4} - [0-9]{1,4} - [ESON] - [ADG]{1,1000}\\s*").matcher(line).matches()) {
                String[] array = line.split(" - ");
                Adventurer adventurer = new Adventurer(array[1], Integer.parseInt(array[2]), Integer.parseInt(array[3]), Orientation.valueOf(array[4]), array[5]);
                try {
                    this.getCell(adventurer.getX(), adventurer.getY()).setBusy(true);
                    adventurer.getTreasure(this.getCell(adventurer.getX(), adventurer.getY()).giveTreasure());
                    adventurers.add(adventurer);
                    if (nbTours < adventurer.getCourse().length())
                        this.nbTours = adventurer.getCourse().length();
                } catch (NullPointerException e) {
                    System.err.println("Commands lines are not correct, A is outside map or there is no map command \"C - {number} - {number}\"");
                }
            } else if (!Pattern.compile("^#.*").matcher(line).matches()) {
                System.err.println("Error parsing line: " + line);
            }
        }
        System.out.println("Parsing file input finish");
    }

    int run() {
        if (cells.size() < 1) {
            System.err.println("There is no map check that there is a command \"C - {number} - {number}\" in the input file");
            return 144;
        }
        System.out.println("The simulation will be started....");
        int i = 0;
        while (i < nbTours) {
            System.out.println("Tour " + (i + 1));
            for (Adventurer adventurer : adventurers) {
                if (adventurer.getCourse().length() > i) {
                    String step = adventurer.getCourse().split("")[i];
                    if (step.equals("A")) {
                        if (this.canMove(adventurer)) {
                            this.getCell(adventurer.getX(), adventurer.getY()).setBusy(false);
                            adventurer.move();
                            adventurer.getTreasure(this.getCell(adventurer.getX(), adventurer.getY()).giveTreasure());
                        }
                    } else if (step.equals("D") || step.equals("G")) {
                        adventurer.turn(Turn.valueOf(step));
                    }
                }

            }
            i = i + 1;
        }
        System.out.println("The simulation is finished");
        this.printResults();
        System.out.println("-----------------TEMMINATED WITH SUCESS-----------------");
        System.out.println("The results were written in the file output");
        return 200;
    }

    private Cell getCell(Integer x, Integer y) {
        if (x < dimX && y < dimY) {
            return cells.get(x + (dimX * y));
        } else {
            return null;
        }
    }

    private Boolean canMove(Adventurer adventurer) {
        Adventurer adventurerTmp = new Adventurer(adventurer.getName(), adventurer.getX(),
                adventurer.getY(), adventurer.getOrientation(),
                adventurer.getCourse());
        adventurerTmp.move();
        if (!(adventurerTmp.getX() < dimX && adventurerTmp.getY() < dimY))
            return false;
        Cell cell = this.getCell(adventurerTmp.getX(), adventurerTmp.getY());
        if (cell != null) {
            return !cell.getBusy() && !cell.getMountain();
        }
        return false;
    }

    private void printResults() {
        System.out.println("Printing results...");
        Utils.createFile("output");
        try {
            FileOutputStream writer = new FileOutputStream("output");
            writer.write(("C - " + dimX + " - " + dimY + "\n").getBytes());
            int index = 0;
            for (Cell cell : cells) {
                if (cell.getMountain())
                    writer.write(("M - " + index % dimX + " - " + index / dimX + "\n").getBytes());
                if (cell.getNbTreasure() > 0)
                    writer.write(("T - " + index % dimX + " - " + index / dimX + " - " + cell.getNbTreasure() + "\n").getBytes());
                index = index + 1;
            }
            for (Adventurer adventurer : adventurers) {
                writer.write(("A - " +
                        adventurer.getName() + " - " +
                        adventurer.getX() + " - " +
                        adventurer.getY() + " - " +
                        adventurer.getOrientation().name() + " - " +
                        adventurer.getNbTreasure() + "\n").getBytes());
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
